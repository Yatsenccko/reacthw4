import React from 'react'
import { ProductList } from '../../components/product/productList'
import Button from '../../components/button'
import { useDispatch } from 'react-redux'
import { deleteAllProductsFromCart } from '../../store/reducers/cartReducer'
import { removeAllProducts } from '../../store/reducers/productsReducer'
const CartPage = () => {
  const dispatch=useDispatch()

  return (
    <>
    <Button text="Удалить товары с корзины" onClick={()=>{
      dispatch(deleteAllProductsFromCart())
      dispatch(removeAllProducts())
      }} styles=""/>
    <ProductList productListId="cart"/>
    </>
    
  )
}

export default CartPage