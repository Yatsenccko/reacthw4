import React from 'react'
import { AiOutlineShoppingCart, AiOutlineHeart, AiFillHome } from 'react-icons/ai';
import { Link } from 'react-router-dom';
import { useSelector } from "react-redux"
import { StyledHeader } from "./styled";
const Header = () => {
  const countCart = useSelector(state=> Object.values(state.cart.cart)) || []
  const fav = useSelector(state=> state.fav) || []
  const elementsInCart = countCart.reduce((acc, current) => {
    acc += current.count
    return acc
  }, 0 )
  return (


    <StyledHeader>

      <Link to="/">
      <AiFillHome />
      </Link>

      <Link to="/favorite">
      <AiOutlineHeart />{fav.fav.length}
      </Link>

      <Link to="/cart">
      <AiOutlineShoppingCart />
      {elementsInCart}
      </Link>

    </StyledHeader>
  )

}

export default Header