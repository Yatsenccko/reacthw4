import './App.css';
import { useEffect } from 'react';
import { getAllProducts } from './store/reducers/productsReducer';
import { useDispatch } from "react-redux";
import LayOut from './pages/layout';
import { getFavFromStorage } from './store/reducers/favReducer';
import { getCartFromStorage } from './store/reducers/cartReducer';
function App() {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getAllProducts());
    dispatch(getFavFromStorage())
    dispatch(getCartFromStorage())
  }, [dispatch])
  return (
    <div className="App">
      <LayOut />
    </div>
  );
}

export default App;
